package de.nidomiro.rxjavademo;

import android.arch.lifecycle.Lifecycle;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jakewharton.rxbinding2.view.RxView;
import com.jakewharton.rxbinding2.widget.RxTextView;
import com.trello.lifecycle2.android.lifecycle.AndroidLifecycle;
import com.trello.rxlifecycle2.LifecycleProvider;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

public class MainActivity extends AppCompatActivity {

    private static String TAG = MainActivity.class.getCanonicalName();

    private final LifecycleProvider<Lifecycle.Event> lifecycleProvider
            = AndroidLifecycle.createLifecycleProvider(this);

    @BindView(R.id.editTextEMail)
    EditText editTextEmail;
    private BehaviorSubject<CharSequence> editTextEmailText = BehaviorSubject.createDefault("");

    @BindView(R.id.editTextPassword)
    EditText editTextPassword;
    private BehaviorSubject<CharSequence> editTextPasswordText = BehaviorSubject.createDefault("");

    @BindView(R.id.buttonLogin)
    Button buttonLogin;
    /**
     * Do not use the value emitted, it is unspecified and only meant as a Notification.
     */
    private PublishSubject<Object> buttonLoginClicked = PublishSubject.create();

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    BehaviorSubject<Boolean> formValid = BehaviorSubject.createDefault(false);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        progressBar.setVisibility(View.GONE);

    }

    @Override
    protected void onResume() {
        super.onResume();

        setupViewBindings();
        setupFormValidation();
        setupLoginButtonBindings();
    }

    /**
     * Redirect events from the Fields to RxJava
     */
    private void setupViewBindings() {
        RxTextView.textChanges(editTextEmail)
                .compose(lifecycleProvider.bindToLifecycle())
                .subscribe(editTextEmailText);

        RxTextView.textChanges(editTextPassword)
                .compose(lifecycleProvider.bindToLifecycle())
                .subscribe(editTextPasswordText);

        RxView.clicks(buttonLogin)
                .compose(lifecycleProvider.bindToLifecycle())
                .subscribe(buttonLoginClicked);
    }

    /**
     * Setting this.formValid according to the FormState
     */
    private void setupFormValidation() {

        Observable.combineLatest(
                editTextEmailText
                        .compose(Utils.charSequenceToTrimedString())
                        .map(EmailFieldState::fromString),

                editTextPasswordText
                        .compose(Utils.charSequenceToTrimedString())
                        .map(PasswordFieldState::fromString),

                (emailFieldState, passwordFieldState) -> {
                    Log.w(TAG, "emailFieldState: " + emailFieldState + " passwordFieldState: " + passwordFieldState);
                    return emailFieldState == EmailFieldState.VALID
                            && passwordFieldState == PasswordFieldState.VALID;
                })
                .compose(lifecycleProvider.bindToLifecycle())
                .subscribe(this.formValid::onNext,
                        error -> Log.wtf(TAG, "Checking for Formvalidation has returned a error ?!", error)
                );
    }

    /**
     * Binding LoginButon to this.formValid. Handle LoginButton press.
     */
    private void setupLoginButtonBindings() {

        formValid
                .onErrorReturnItem(false)
                .compose(lifecycleProvider.bindToLifecycle())
                .subscribe(valid -> {
                    buttonLogin.setEnabled(valid);
                    buttonLogin.setBackgroundColor((valid)? Color.RED: Color.GRAY);
                });

        buttonLoginClicked
                .compose(checkLoginCredentials())
                .compose(delayOperation(5))

                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(uuid -> progressBar.setVisibility(View.VISIBLE))

                .compose(requestEmail())

                .observeOn(AndroidSchedulers.mainThread())
                .compose(lifecycleProvider.bindToLifecycle())
                .subscribe(
                        email -> {
                            progressBar.setVisibility(View.GONE);
                            displayUserMessage("The email was: " + email);
                        },
                        error -> {
                            progressBar.setVisibility(View.GONE);
                            displayUserError(error);
                        }
                );

    }

    private ObservableTransformer<Object, UUID> checkLoginCredentials(){
        return source -> source
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext(o -> progressBar.setVisibility(View.VISIBLE))
                .flatMap(o -> FancyRestService // Check credentials against the REST-Service
                        .get()
                        .tokenForUser(
                                editTextEmailText.getValue().toString(),
                                editTextPasswordText.getValue().toString()
                        )
                        .toObservable()
                        .subscribeOn(Schedulers.io()) // Execute on IO Thread-Pool
                )
                .observeOn(AndroidSchedulers.mainThread()) // Move back to the mainThread because of UI interaction
                .retry( error -> { // On error display it and Retry execution
                    progressBar.setVisibility(View.GONE);
                    displayUserError(error);
                    return true;
                })
                .doOnNext(uuid -> {
                    progressBar.setVisibility(View.GONE);
                    Log.w(TAG, "Login Success: token: " + uuid);
                    Toast.makeText(this, "Logged in successfully", Toast.LENGTH_SHORT).show();
                });
    }

    private <T> ObservableTransformer<T, T> delayOperation(int i) {
        return source -> source
                .doOnNext(value -> {
                    displayUserMessage("delaying operation for " + i + "s");
                })
                .delay(i, TimeUnit.SECONDS) // Automatically runs in Schedulers.computation()
                ;
    }

    private ObservableTransformer<UUID, String> requestEmail() {
        return source -> source
                .flatMap(uuid ->
                        FancyRestService
                                .get()
                                .getUserEmail(uuid)
                                .toObservable()
                                .subscribeOn(Schedulers.io())
                )
                ;

    }


    private void displayUserMessage(String message) {
        Log.w(TAG, message);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void displayUserError(Throwable error) {
        if (error instanceof FancyRestService.NoValidCredentialsException) {
            Log.w(TAG, "Error: No valid credentials");
            Toast.makeText(this, "Error: No valid credentials", Toast.LENGTH_SHORT).show();
        }else {
            Log.e(TAG, "Unknown Error: ", error);
            Toast.makeText(this, "Unknown Error: " + error.getClass().getSimpleName(), Toast.LENGTH_LONG).show();
        }
    }

}

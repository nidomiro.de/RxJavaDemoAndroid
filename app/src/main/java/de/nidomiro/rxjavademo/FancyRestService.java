package de.nidomiro.rxjavademo;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * This is a very fancy RestService (for demonstrations, never use this class in production!)
 */

public class FancyRestService {

    private Map<UUID, String> userEmails = new ConcurrentHashMap<>();
    private Map<String, String> registeredUsers = new ConcurrentHashMap<>();

    private FancyRestService() {
        registeredUsers.put("test1@mischok-it.de", "111111");
        registeredUsers.put("test0@mischok-it.de", "000000");
    }

    public Single<UUID> tokenForUser(String email, String password) {
        return Single.create(source -> {
            Thread.sleep(2000); // Slow Rest Service
            if ( password.equals(registeredUsers.get(email)) ) {
                UUID token = UUID.randomUUID();
                userEmails.put(token, email);
                source.onSuccess(token);
            } else {
                source.onError(new NoValidCredentialsException());
            }
        });
    }

    public Single<String> getUserEmail(UUID accessToken) {
        return Single.create(source -> {
            Thread.sleep(2000); // Slow Rest Service
            String email = userEmails.get(accessToken);
            if ( email != null) {
                source.onSuccess(email);
            } else {
                source.onError(new NoValidCredentialsException());
            }
        });
    }


    public static class NoValidCredentialsException extends Exception {}


    // Singleton stuff -> evil, but necessary for the quick demonstration
    private static FancyRestService instance;
    public static FancyRestService get() {
        if(instance == null){
            instance = new FancyRestService();
        }
        return instance;
    }

}

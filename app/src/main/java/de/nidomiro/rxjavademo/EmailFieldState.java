package de.nidomiro.rxjavademo;

/**
 * TODO: Add a class header comment!
 */
public enum EmailFieldState {
    VALID,
    TOO_SHORT;

    public static EmailFieldState fromString(String email) {
        if (Utils.checkMinCharCount(email, 5)) {
            return TOO_SHORT;
        } else {
            return VALID;
        }
    }
}

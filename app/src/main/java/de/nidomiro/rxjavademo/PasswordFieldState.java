package de.nidomiro.rxjavademo;

/**
 * TODO: Add a class header comment!
 */
public enum PasswordFieldState {
    VALID,
    TOOSHORT;

    public static PasswordFieldState fromString(String email) {
        if (Utils.checkMinCharCount(email, 6)) {
            return TOOSHORT;
        } else {
            return VALID;
        }
    }
}

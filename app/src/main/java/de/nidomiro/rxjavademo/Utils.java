package de.nidomiro.rxjavademo;


import io.reactivex.ObservableTransformer;

public abstract class Utils {


    public static boolean checkMinCharCount(String value, int count) {
        return value.length() < count;
    }

    public static ObservableTransformer<CharSequence, String> charSequenceToTrimedString() {
        return source -> source.
                map(CharSequence::toString)
                .map(String::trim);
    }
}
